﻿using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using GalleryServices.Services;
using GalleryServices.ServicesInterfaces;
using GalleryStorage;
using GalleryStorage.Models;
using GalleryStorage.Storages;
using GalleryStorage.StoragesInterfaces;
using Microsoft.AspNetCore.Identity;

namespace StatisticsWebJob
{
    class Program
    {
        private static IConfiguration _configuration;

        static void Main()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            _configuration = builder.Build();

            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var configuration = new JobHostConfiguration();
            configuration.UseDevelopmentSettings();
            configuration.JobActivator = new CustomJobActivator(serviceCollection.BuildServiceProvider());
            configuration.UseTimers();

            var host = new JobHost(configuration);
            host.RunAndBlock();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(_configuration);

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<GalleryContext>()
                .AddDefaultTokenProviders();

            string connection = _configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<GalleryContext>(options =>
                options.UseSqlServer(connection));

            services.AddTransient<IEmailServices, EmailServices>();

            services.AddTransient<IUserStorage<User>, UserStorage>();
            services.AddTransient<IUserServices<User>, UserServices>();

            services.AddTransient<IStorage<Album>, AlbumStorage>();
            services.AddTransient<IAlbumServices<Album>, AlbumServices>();

            services.AddTransient<IStorage<Picture>, PictureStorage>();
            services.AddTransient<IPictureServices<Picture>, PictureServices>();

            services.AddTransient<IStorage<Rating>, RatingStorage>();
            services.AddTransient<IServices<Rating>, RatingServices>();

            services.AddTransient<IStorage<Comment>, CommentStorage>();
            services.AddTransient<IServices<Comment>, CommentServices>();

            services.AddTransient<WebJobsMethods, WebJobsMethods>();


            Environment.SetEnvironmentVariable("AzureWebJobsDashboard", _configuration["AzureWebJobsDashboard"]);
            Environment.SetEnvironmentVariable("AzureWebJobsStorage", _configuration["AzureWebJobsStorage"]);
        }
    }
}
