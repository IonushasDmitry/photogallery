﻿using Microsoft.Azure.WebJobs;
using System;
using System.IO;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;

namespace StatisticsWebJob
{
    public class WebJobsMethods
    {
        private readonly IUserServices<User> _userServices;
        private readonly IEmailServices _emailServices;

        public WebJobsMethods(IUserServices<User> userServices, IEmailServices emailServices)
        {
            _userServices = userServices;
            _emailServices = emailServices;
        }

        // The timer is triggered once a month
        public async Task SendStatistics([TimerTrigger("0 0 0 1 */1 *")]TimerInfo timer, TextWriter log)
        {
            Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            Console.WriteLine("Send statistics");
            Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

            var users = await _userServices.GetAllAsync();
            foreach (var user in users)
            {
                int comments = 0;
                int rates = 0;
                if (user.Albums == null)
                {
                    continue;
                }
                foreach (var album in user.Albums)
                {
                    if (album.Pictures == null)
                    {
                        continue;
                    }
                    foreach (var picture in album.Pictures)
                    {
                        if (picture.Comments == null)
                        {
                            continue;
                        }
                        foreach (var comment in picture.Comments)
                        {
                            DateTime today = DateTime.Today;
                            if ((today - comment.PostDateTime).TotalDays <=
                                DateTime.DaysInMonth(today.Year, today.Month - 1))
                            {
                                comments++;
                            }
                        }

                        foreach (var rating in picture.Ratings)
                        {
                            DateTime today = DateTime.Today;
                            if ((today - rating.RateDateTime).TotalDays <=
                                DateTime.DaysInMonth(today.Year, today.Month - 1))
                            {
                                rates++;
                            }
                        }
                    }

                    string message = "";
                    if (comments > 0)
                    {
                        message += $"Comments to your works in this month: {comments}!\n";
                    }

                    if (rates > 0)
                    {
                        message += $"Rates to your works in this month: {rates}!";
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        await _emailServices.SendEmailAsync(user.Email, "Monthly statistics",
                            message);
                    }
                }
            }
        }
    }
}
