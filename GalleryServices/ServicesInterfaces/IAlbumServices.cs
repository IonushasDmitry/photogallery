﻿using System.Threading.Tasks;

namespace GalleryServices.ServicesInterfaces
{
    public interface IAlbumServices<T>: IServices<T>
    {
        void CreateDirectory(T album);
        string GetDirectoryName(T album);
        string GetRelativePath(T album);
        string GetFullPath(T album);
        Task UpdateAsync(T album);
    }
}
