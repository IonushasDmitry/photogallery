﻿using System.Threading.Tasks;

namespace GalleryServices.ServicesInterfaces
{
    public interface IEmailServices
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
