﻿using System.Threading.Tasks;

namespace GalleryServices.ServicesInterfaces
{
    public interface ITagServices<T>: IServices<T>
    {
        Task<T> GetByNameAsync(string name);
    }
}
