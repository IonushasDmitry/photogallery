﻿using System.Threading.Tasks;
using GalleryStorage.Models;

namespace GalleryServices.ServicesInterfaces
{
    public interface IPictureServices<T>: IServices<T>
    {
        string GetFileName(Picture picture);
        string GetRelativePath(Picture picture);
        string GetFullPath(Picture picture);
        string GetThumbnailFullPath(Picture picture);
        string GetThumbnailRelativePath(Picture picture);
        string GetThumbnailFullDirectoryPath(Picture picture);
        string GetThumbnailRelativeDirectoryPath(Picture picture);
        void CreateThumbnail(Picture picture);
        Task UpdateAsync(Picture picture);
    }
}
