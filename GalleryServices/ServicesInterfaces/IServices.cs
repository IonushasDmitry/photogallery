﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GalleryServices.ServicesInterfaces
{
    public interface IServices<T>
    {
        Task<T> GetAsync(Guid id);
        Task<ICollection<T>> GetAllAsync();
        Task AddAsync(T item);
        Task DeleteAsync(Guid id);
        Task DeleteAsync(T album);
        Task<int> CapacityAsync();
    }
}
