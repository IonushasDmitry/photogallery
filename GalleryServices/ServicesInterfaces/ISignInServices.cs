﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace GalleryServices.ServicesInterfaces
{
    public interface ISignInServices<T> where T : IdentityUser<Guid>
    {
        Task SignInAsync(T user, bool isPersistent);
        Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure);
        Task SignOutAsync();
    }
}
