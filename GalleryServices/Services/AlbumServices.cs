﻿using System;
using System.IO;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;

namespace GalleryServices.Services
{
    public class AlbumServices : AbstractServices<Album>, IAlbumServices<Album>
    {
        private readonly IUserServices<User> _userServices;

        public AlbumServices(IUserServices<User> userServices, IStorage<Album> storage)
            : base(storage)
        {
            _userServices = userServices;
        }

        private void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        public void CreateDirectory(Album album)
        {
            Directory.CreateDirectory(GetFullPath(album));
        }

        public string GetDirectoryName(Album album)
        {
            return album.Id.ToString();
        }

        public string GetRelativePath(Album album)
        {
            return Path.Combine(_userServices.GetRelativePath(album.User), GetDirectoryName(album));
        }

        public string GetFullPath(Album album)
        {
            return Path.Combine(_userServices.GetFullPath(album.User), GetDirectoryName(album));
        }

        public override async Task AddAsync(Album album)
        {
            CreateDirectory(album);
            await Storage.AddAsync(album);
        }

        public override async Task DeleteAsync(Album album)
        {
            string fullPath = GetFullPath(album);
            await base.DeleteAsync(album);
            DeleteDirectory(fullPath);
        }

        public override async Task DeleteAsync(Guid id)
        {
            Album album = await GetAsync(id);
            await DeleteAsync(album);
        }

        public async Task UpdateAsync(Album album)
        {
            await Storage.UpdateAsync(album);
        }
    }
}
