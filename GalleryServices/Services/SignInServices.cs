﻿using GalleryStorage.Models;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;

namespace GalleryServices.Services
{
    public class SignInServices: ISignInServices<User>
    {
        private readonly SignInManager<User> _signInManager;

        public SignInServices(SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
        }

        public async Task SignInAsync(User user, bool isPersistent)
        {
            await _signInManager.SignInAsync(user, isPersistent);
        }

        public async Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            return await _signInManager.PasswordSignInAsync(userName, password, isPersistent, lockoutOnFailure);
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }
    }
}
