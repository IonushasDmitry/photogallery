﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace GalleryServices.Services
{
    public class UserServices : IUserServices<User>
    {
        private readonly IConfiguration _configuration;
        private readonly IUserStorage<User> _userStorage;
        private readonly string _rootPath;

        public UserServices(IConfiguration configuration, IUserStorage<User> userStorage)
        {
            _configuration = configuration;
            _rootPath = configuration["RootPath:Path"];
            _userStorage = userStorage;
        }

        private void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        public string GetDirectoryName(User user)
        {
            return user.Id.ToString();
        }

        public string GetRelativePath(User user)
        {
            return Path.Combine("/Files/", GetDirectoryName(user));
        }

        public string GetFullPath(User user)
        {
            string path = Path.Combine(_rootPath, "Files", GetDirectoryName(user));
            return path;
        }

        public async Task<IdentityResult> CreateAsync(User user, string password)
        {
            return await _userStorage.CreateAsync(user, password);
        }

        public async Task<int> CapacityAsync()
        {
            return await _userStorage.CapacityAsync();
        }

        public async Task DeleteAsync(User user)
        {
            if (Directory.Exists(GetFullPath(user)))
            {
                DeleteDirectory(GetFullPath(user));
            }
            await _userStorage.DeleteAsync(user);
        }

        public async Task<IdentityResult> UpdateAsync(User user)
        {
            return await _userStorage.UpdateAsync(user);
        }

        public async Task<User> FindByIdAsync(Guid id)
        {
            return await _userStorage.FindByIdAsync(id);
        }

        public async Task<User> FindByNameAsync(string name)
        {
            return await _userStorage.FindByNameAsync(name);
        }

        public async Task<ICollection<User>> GetAllAsync()
        {
            return await _userStorage.GetAllAsync();
        }

        public async Task<IdentityResult> SetLockOutEnabledAsync(User user, bool enabled)
        {
            return await _userStorage.SetLockOutEnabledAsync(user, enabled);
        }

        public async Task<IdentityResult> SetLockOutEndDateAsync(User user, DateTimeOffset? lockoutEnd)
        {
            return await _userStorage.SetLockoutEndDateAsync(user, lockoutEnd);
        }

        public async Task<bool> IsLockedOutAsync(User user)
        {
            return await _userStorage.IsLockedOutAsync(user);
        }

        public async Task<IdentityResult> ResetAccessFailedCountAsync(User user)
        {
            return await _userStorage.ResetAccessFailedCountAsync(user);
        }

        public async Task<IdentityResult> ChangePasswordAsync(User user, string currentPassword, string newPassword)
        {
            return await _userStorage.ChangePasswordAsync(user, currentPassword, newPassword);
        }

        public async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            return await _userStorage.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> AddToRolesAsync(User user, IEnumerable<string> roles)
        {
            return await _userStorage.AddToRolesAsync(user, roles);
        }

        public async Task<IdentityResult> RemoveFromRolesAsync(User user, IEnumerable<string> roles)
        {
            return await _userStorage.RemoveFromRolesAsync(user, roles);
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await _userStorage.GetRolesAsync(user);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            return await _userStorage.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(User user, string token)
        {
            return await _userStorage.ConfirmEmailAsync(user, token);
        }

        public async Task<bool> IsEmailConfirmedAsync(User user)
        {
            return await _userStorage.IsEmailConfirmedAsync(user);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            return await _userStorage.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string token, string newPassword)
        {
            return await _userStorage.ResetPasswordAsync(user, token, newPassword);
        }
    }
}
