﻿using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;

namespace GalleryServices.Services
{
    public class CommentServices : AbstractServices<Comment>
    {
        public CommentServices(IStorage<Comment> storage)
            : base(storage)
        { }
    }
}
