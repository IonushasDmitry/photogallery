﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.AspNetCore.Identity;

namespace GalleryServices.Services
{
    public class RoleServices: IRoleServices<Role>
    {
        private readonly IRoleStorage<Role> _roleStorage;

        public RoleServices(IRoleStorage<Role> roleStorage)
        {
            _roleStorage = roleStorage;
        }

        public IList<Role> GetAll()
        {
            return _roleStorage.GetAll();
        }

        public async Task<IdentityResult> CreateAsync(Role role)
        {
            return await _roleStorage.CreateAsync(role);
        }

        public async Task<Role> FindByNameAsync(string name)
        {
            return await _roleStorage.FindByNameAsync(name);
        }

        public Role FindById(Guid id)
        {
            return _roleStorage.FindById(id);
        }

        public async Task<IdentityResult> DeleteAsync(Role role)
        {
            return await _roleStorage.DeleteAsync(role);
        }
    }
}
