﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.StoragesInterfaces;

namespace GalleryServices.Services
{
    public abstract class AbstractServices<T> : IServices<T>
    {
        protected readonly IStorage<T> Storage;

        protected AbstractServices(IStorage<T> storage)
        {
            Storage = storage;
        }

        public virtual async Task AddAsync(T item)
        {
            await Storage.AddAsync(item);
        }

        public virtual async Task<int> CapacityAsync()
        {
            return await Storage.CapacityAsync();
        }

        public virtual async Task DeleteAsync(Guid id)
        {
            await Storage.DeleteAsync(id);
        }

        public virtual async Task DeleteAsync(T item)
        {
            await Storage.DeleteAsync(item);
        }

        public virtual async Task<T> GetAsync(Guid id)
        {
            return await Storage.GetAsync(id);
        }

        public virtual async Task<ICollection<T>> GetAllAsync()
        {
            return await Storage.GetAllAsync();
        }
    }
}
