﻿using GalleryStorage.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.StoragesInterfaces;

namespace GalleryServices.Services
{
    public class TagServices : ITagServices<Tag>
    {
        private readonly ITagStorage<Tag> _tagStorage;

        public TagServices(ITagStorage<Tag> tagStorage)
        {
            _tagStorage = tagStorage;
        }

        public async Task<Tag> GetAsync(Guid id)
        {
            return await _tagStorage.GetAsync(id);
        }

        public async Task<Tag> GetByNameAsync(string name)
        {
            return await _tagStorage.GetByNameAsync(name);
        }

        public async Task<ICollection<Tag>> GetAllAsync()
        {
            return await _tagStorage.GetAllAsync();
        }

        public async Task AddAsync(Tag tag)
        {
            await _tagStorage.AddAsync(tag);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _tagStorage.DeleteAsync(id);
        }

        public async Task DeleteAsync(Tag tag)
        {
            await _tagStorage.DeleteAsync(tag);
        }

        public async Task UpdateAsync(Tag tag)
        {
            await _tagStorage.UpdateAsync(tag);
        }

        public async Task<int> CapacityAsync()
        {
            return await _tagStorage.CapacityAsync();
        }

        public List<Tag> Where(Func<Tag, bool> predicate)
        {
            return _tagStorage.Where(predicate);
        }
    }
}
