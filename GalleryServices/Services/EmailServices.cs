﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using Microsoft.Extensions.Configuration;

namespace GalleryServices.Services
{
    public class EmailServices: IEmailServices
    {
        private readonly IConfiguration _configuration;
        private readonly string _emailFrom;
        private readonly string _password;
        public EmailServices(IConfiguration configuration)
        {
            _configuration = configuration;
            _emailFrom = _configuration["Email:Address"];
            _password = _configuration["Email:Password"];
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Gallery administration", _emailFrom));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587);
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                await client.AuthenticateAsync(_emailFrom, _password);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
