﻿using System;
using System.IO;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using SkiaSharp;

namespace GalleryServices.Services
{
    public class PictureServices : AbstractServices<Picture>, IPictureServices<Picture>
    {
        private readonly IAlbumServices<Album> _albumServices;

        // TODO: get values from config
        private const string ThumbnailDirectoryName = "thumbnail";
        private const int Size = 350;
        private const int Quality = 75;

        public PictureServices(IStorage<Picture> storage, IAlbumServices<Album> albumServices)
            : base(storage)
        {
            _albumServices = albumServices;
        }

        public string GetFileName(Picture picture)
        {
            return picture.FileName;
        }

        public string GetRelativePath(Picture picture)
        {
            return Path.Combine(_albumServices.GetRelativePath(picture.Album), GetFileName(picture));
        }

        public string GetThumbnailRelativePath(Picture picture)
        {
            return Path.Combine(GetThumbnailRelativeDirectoryPath(picture), GetFileName(picture));
        }

        public string GetFullPath(Picture picture)
        {
            return Path.Combine(_albumServices.GetFullPath(picture.Album), GetFileName(picture));
        }

        public string GetThumbnailFullPath(Picture picture)
        {
            return Path.Combine(GetThumbnailFullDirectoryPath(picture), GetFileName(picture));
        }

        public string GetThumbnailRelativeDirectoryPath(Picture picture)
        {
            return Path.Combine(_albumServices.GetRelativePath(picture.Album), ThumbnailDirectoryName);
        }

        public string GetThumbnailFullDirectoryPath(Picture picture)
        {
            return Path.Combine(_albumServices.GetFullPath(picture.Album), ThumbnailDirectoryName);
        }

        public override async Task AddAsync(Picture picture)
        {
            string albumPath = _albumServices.GetFullPath(picture.Album);
            if (!Directory.Exists(albumPath))
            {
                Directory.CreateDirectory(albumPath);
            }

            await base.AddAsync(picture);
        }

        public override async Task DeleteAsync(Guid id)
        {
            await Storage.DeleteAsync(await GetAsync(id));
        }

        public override async Task DeleteAsync(Picture picture)
        {
            if (File.Exists(GetFullPath(picture)))
            {
                File.Delete(GetFullPath(picture));
            }

            if (File.Exists(GetThumbnailFullPath(picture)))
            {
                File.Delete(GetThumbnailFullPath(picture));
            }

            await Storage.DeleteAsync(picture);
        }

        public async Task UpdateAsync(Picture picture)
        {
            await Storage.UpdateAsync(picture);
        }

        // Use SkiaSharp because ImageSharp doesn't work (errors in .net core)
        public void CreateThumbnail(Picture picture)
        {
            string picturePath = GetFullPath(picture);
            string thumbnailPath = GetThumbnailFullPath(picture);

            if (!Directory.Exists(GetThumbnailFullDirectoryPath(picture)))
            {
                Directory.CreateDirectory(GetThumbnailFullDirectoryPath(picture));
            }

            using (var input = File.OpenRead(picturePath))
            {
                using (var inputStream = new SKManagedStream(input))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        if (original.ColorType != SKImageInfo.PlatformColorType)
                        {
                            original.CopyTo(original, SKImageInfo.PlatformColorType);
                        }

                        int minSize = Math.Min(original.Width, original.Height);
                        int startX = 0;
                        int startY = 0;
                        int shift = (Math.Max(original.Width, original.Height) - minSize) / 2;
                        if (original.Width > original.Height)
                        {
                            startX = shift;
                        }
                        else
                        {
                            startY = shift;
                        }

                        var cropedImage = SKImage.FromBitmap(original);
                        cropedImage = cropedImage.Subset(SKRectI.Create(startX, startY, minSize, minSize));
                        var cropedBitmap = SKBitmap.FromImage(cropedImage);

                        using (var resized = cropedBitmap.Resize(new SKImageInfo(Size, Size), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null)
                            {
                                return;
                            }

                            using (var image = SKImage.FromBitmap(resized))
                            {
                                using (var output = File.OpenWrite(thumbnailPath))
                                {
                                    // TODO: if image is not jpeg?
                                    image.Encode(SKEncodedImageFormat.Jpeg, Quality).SaveTo(output);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
