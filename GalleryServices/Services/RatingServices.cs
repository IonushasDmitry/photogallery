﻿using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;

namespace GalleryServices.Services
{
    public class RatingServices : AbstractServices<Rating>
    {
        public RatingServices(IStorage<Rating> storage)
            : base(storage)
        { }
    }
}
