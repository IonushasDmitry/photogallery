﻿using Microsoft.EntityFrameworkCore;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace GalleryStorage
{
    public class GalleryContext: IdentityDbContext<User, Role, Guid>
    {
        public DbSet<Album> Albums { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public GalleryContext(DbContextOptions<GalleryContext> options)
                : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasIndex(x => x.Pseudonym)
                .IsUnique();

            modelBuilder.Entity<PictureTag>()
                .HasKey(x => new {x.PictureId, x.TagId});

            modelBuilder.Entity<PictureTag>()
                .HasOne(x => x.Picture)
                .WithMany(x => x.PictureTags)
                .HasForeignKey(x => x.PictureId);

            modelBuilder.Entity<PictureTag>()
                .HasOne(x => x.Tag)
                .WithMany(x => x.PictureTags)
                .HasForeignKey(x => x.TagId);
        }
    }
}
