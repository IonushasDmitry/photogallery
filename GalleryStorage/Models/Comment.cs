﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GalleryStorage.Models
{
    public class Comment
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required]
        public string Context { get; set; }
        [Required]
        public DateTime PostDateTime { get; set; }

        public Guid UserId { get; set; }

        [Required]
        public Guid PictureId { get; set; }
        public Picture Picture { get; set; }
    }
}
