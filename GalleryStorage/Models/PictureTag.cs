﻿using System;

namespace GalleryStorage.Models
{
    public class PictureTag
    {
        public Guid PictureId { get; set; }
        public Picture Picture { get; set; }

        public Guid TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
