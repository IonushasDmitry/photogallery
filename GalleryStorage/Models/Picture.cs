﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GalleryStorage.Models
{
    public class Picture
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required]
        public string Name { get; set; }
        [Required]
        public string FileName { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public DateTime PostDateTime { get; set; }

        public Guid UserId { get; set; }

        [Required]
        public Guid AlbumId { get; set; }
        public Album Album { get; set; }

        public virtual IList<Rating> Ratings { get; set; }
        public virtual IList<Comment> Comments { get; set; }
        public virtual IList<PictureTag> PictureTags { get; set; } = new List<PictureTag>();
    }
}
