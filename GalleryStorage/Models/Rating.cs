﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GalleryStorage.Models
{
    public class Rating
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        public DateTime RateDateTime { get; set; } = DateTime.Now;

        public Guid UserId { get; set; }

        [Required]
        public Guid PictureId { get; set; }
        public Picture Picture { get; set; }
    }
}
