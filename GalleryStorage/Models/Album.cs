﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GalleryStorage.Models
{
    public class Album
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }

        [Required]
        public Guid UserId { get; set; }
        public User User { get; set; }

        public virtual IList<Picture> Pictures{ get; set; }
    }
}
