﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GalleryStorage.Models
{
    public class Tag
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required]
        public string Name { get; set; }

        public IList<PictureTag> PictureTags { get; set; } = new List<PictureTag>();
    }
}
