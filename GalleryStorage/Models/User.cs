﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GalleryStorage.Models
{
    public class User: IdentityUser<Guid>
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Pseudonym { get; set; }
        public string Address { get; set; }
        public string BlockReason { get; set; }
        public DateTime DateOfPasswordChange { get; set; }

        public virtual IList<Album> Albums { get; set; }
    }
}
