﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace GalleryStorage.Storages
{
    public class UserStorage : IUserStorage<User>
    {
        private readonly UserManager<User> _userManager;
        private readonly GalleryContext _context;

        public UserStorage(UserManager<User> userManager, GalleryContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<User> FindByIdAsync(Guid id)
        {
            return await _context.Users.Include(x => x.Albums).ThenInclude(x => x.Pictures).ThenInclude(x => x.Comments)
                                        .Include(x => x.Albums).ThenInclude(x => x.Pictures).ThenInclude(x => x.Ratings)
                                        .SingleOrDefaultAsync(m => m.Id.Equals(id));
        }

        public async Task<User> FindByNameAsync(string name)
        {
            return await _context.Users.Include(x => x.Albums).ThenInclude(x => x.Pictures).ThenInclude(x => x.Comments)
                                        .Include(x => x.Albums).ThenInclude(x => x.Pictures).ThenInclude(x => x.Ratings)
                                        .SingleOrDefaultAsync(x => string.Equals(x.UserName, name));
        }

        public async Task<ICollection<User>> GetAllAsync()
        {
            return await _userManager.Users.Include(x => x.Albums).ThenInclude(x => x.Pictures).ThenInclude(x => x.Comments)
                                            .Include(x => x.Albums).ThenInclude(x => x.Pictures).ThenInclude(x => x.Ratings)
                                            .ToListAsync();
        }

        public async Task<IdentityResult> CreateAsync(User user, string password)
        {
            user.Id = Guid.NewGuid();
            return await _userManager.CreateAsync(user, password);
        }

        public async Task DeleteAsync(User user)
        {
            await _userManager.DeleteAsync(user);
        }

        public async Task<IdentityResult> UpdateAsync(User user)
        {
            return await _userManager.UpdateAsync(user);
        }

        public async Task<int> CapacityAsync()
        {
            return await _userManager.Users.CountAsync();
        }

        public async Task<IdentityResult> SetLockOutEnabledAsync(User user, bool enabled)
        {
            return await _userManager.SetLockoutEnabledAsync(user, enabled);
        }

        public async Task<IdentityResult> SetLockoutEndDateAsync(User user, DateTimeOffset? lockoutEnd)
        {
            return await _userManager.SetLockoutEndDateAsync(user, lockoutEnd);
        }

        public async Task<bool> IsLockedOutAsync(User user)
        {
            return await _userManager.IsLockedOutAsync(user);
        }

        public async Task<IdentityResult> ResetAccessFailedCountAsync(User user)
        {
            return await _userManager.ResetAccessFailedCountAsync(user);
        }

        public async Task<IdentityResult> ChangePasswordAsync(User user, string currentPassword, string newPassword)
        {
            return await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
        }

        public async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> AddToRolesAsync(User user, IEnumerable<string> roles)
        {
            return await _userManager.AddToRolesAsync(user, roles);
        }

        public async Task<IdentityResult> RemoveFromRolesAsync(User user, IEnumerable<string> roles)
        {
            return await _userManager.RemoveFromRolesAsync(user, roles);
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await _userManager.GetRolesAsync(user);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(User user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task<bool> IsEmailConfirmedAsync(User user)
        {
            return await _userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string token, string newPassword)
        {
            return await _userManager.ResetPasswordAsync(user, token, newPassword);
        }
    }
}
