﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.EntityFrameworkCore;

namespace GalleryStorage.Storages
{
    public class TagStorage : ITagStorage<Tag>
    {
        private readonly GalleryContext _context;

        public TagStorage(GalleryContext context)
        {
            _context = context;
        }

        public async Task<Tag> GetAsync(Guid id)
        {
            return await _context.Tags.SingleOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<Tag> GetByNameAsync(string name)
        {
            return await _context.Tags.SingleOrDefaultAsync(x => x.Name.Equals(name));
        }

        public async Task<ICollection<Tag>> GetAllAsync()
        {
            return await _context.Tags.ToListAsync();
        }

        public async Task AddAsync(Tag tag)
        {
            await _context.Tags.AddAsync(tag);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            Tag tag = await _context.Tags.SingleOrDefaultAsync(x => x.Id.Equals(id));
            if (tag == null)
            {
                return;
            }

            _context.Entry(tag).State = EntityState.Detached;
            _context.Entry(tag).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Tag tag)
        {
            if (tag == null)
            {
                return;
            }

            _context.Entry(tag).State = EntityState.Detached;
            _context.Entry(tag).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Tag tag)
        {
            _context.Tags.Update(tag);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CapacityAsync()
        {
            return await _context.Tags.CountAsync();
        }

        public List<Tag> Where(Func<Tag, bool> predicate)
        {
            return _context.Tags.Where(predicate).ToList();
        }
    }
}
