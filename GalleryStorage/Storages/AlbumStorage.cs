﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.EntityFrameworkCore;

namespace GalleryStorage.Storages
{
    public class AlbumStorage : IStorage<Album>
    {
        private readonly GalleryContext _context;

        public AlbumStorage(GalleryContext context)
        {
            _context = context;
        }

        public async Task<Album> GetAsync(Guid id)
        {
            return await _context.Albums.Include(x => x.User).Include(x => x.Pictures).ThenInclude(x => x.Ratings).SingleOrDefaultAsync(m => m.Id.Equals(id));
        }

        public async Task<ICollection<Album>> GetAllAsync()
        {
            return await _context.Albums.Include(x => x.Pictures).ThenInclude(x => x.Ratings).ToListAsync();
        }

        public async Task AddAsync(Album album)
        {
            await _context.Albums.AddAsync(album);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            Album album = await _context.Albums.Include(x => x.Pictures).ThenInclude(x => x.Ratings).SingleOrDefaultAsync(m => m.Id.Equals(id));
            if (album == null)
            {
                return;
            }

            _context.Entry(album).State = EntityState.Detached;
            _context.Entry(album).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Album album)
        {
            if (album == null)
            {
                return;
            }

            _context.Entry(album).State = EntityState.Detached;
            _context.Entry(album).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Album album)
        {
            _context.Albums.Update(album);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CapacityAsync()
        {
            return await _context.Albums.CountAsync();
        }

        public List<Album> Where(Func<Album, bool> predicate)
        {
            return _context.Albums.Include(x => x.Pictures).ThenInclude(x => x.Ratings).Where(predicate).ToList();
        }
    }
}
