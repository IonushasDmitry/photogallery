﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.AspNetCore.Identity;

namespace GalleryStorage.Storages
{
    public class RoleStorage: IRoleStorage<Role>
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly GalleryContext _context;

        public RoleStorage(RoleManager<Role> roleManager, GalleryContext context)
        {
            _roleManager = roleManager;
            _context = context;
        }

        public IList<Role> GetAll()
        {
            return _roleManager.Roles.ToList();
        }

        public async Task<IdentityResult> CreateAsync(Role role)
        {
            return await _roleManager.CreateAsync(role);
        }

        public async Task<Role> FindByNameAsync(string name)
        {
            return await _roleManager.FindByNameAsync(name);
        }

        public Role FindById(Guid id)
        {
            return _context.Roles.SingleOrDefault(x => x.Id.Equals(id));
        }

        public async Task<IdentityResult> DeleteAsync(Role role)
        {
            return await _roleManager.DeleteAsync(role);
        }
    }
}
