﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.EntityFrameworkCore;

namespace GalleryStorage.Storages
{
    public class CommentStorage : IStorage<Comment>
    {
        private readonly GalleryContext _context;

        public CommentStorage(GalleryContext context)
        {
            _context = context;
        }

        public async Task<Comment> GetAsync(Guid id)
        {
            return await _context.Comments.Include(x => x.Picture).SingleOrDefaultAsync(m => m.Id.Equals(id));
        }

        public async Task<ICollection<Comment>> GetAllAsync()
        {
            return await _context.Comments.Include(x => x.Picture).OrderByDescending(x => x.PostDateTime).ToListAsync();
        }

        public async Task AddAsync(Comment comment)
        {
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            Comment comment = await _context.Comments.SingleOrDefaultAsync(m => m.Id.Equals(id));
            await DeleteAsync(comment);
        }

        public async Task DeleteAsync(Comment comment)
        {
            if (comment == null)
            {
                return;
            }

            _context.Entry(comment).State = EntityState.Detached;
            _context.Entry(comment).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Comment comment)
        {
            _context.Comments.Update(comment);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CapacityAsync()
        {
            return await _context.Comments.CountAsync();
        }

        public List<Comment> Where(Func<Comment, bool> predicate)
        {
            return _context.Comments.Where(predicate).ToList();
        }
    }
}
