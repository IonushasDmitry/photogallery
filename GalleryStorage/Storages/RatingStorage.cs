﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.EntityFrameworkCore;

namespace GalleryStorage.Storages
{
    public class RatingStorage : IStorage<Rating>
    {
        private readonly GalleryContext _context;

        public RatingStorage(GalleryContext context)
        {
            _context = context;
        }

        public async Task<Rating> GetAsync(Guid id)
        {
            return await _context.Ratings.Include(x => x.Picture).SingleOrDefaultAsync(m => m.Id.Equals(id));
        }

        public async Task<ICollection<Rating>> GetAllAsync()
        {
            return await _context.Ratings.ToListAsync();
        }

        public async Task AddAsync(Rating rating)
        {
            await _context.Ratings.AddAsync(rating);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            Rating rating = await _context.Ratings.SingleOrDefaultAsync(m => m.Id.Equals(id));
            await DeleteAsync(rating);
        }

        public async Task DeleteAsync(Rating rating)
        {
            if (rating == null)
            {
                return;
            }

            _context.Entry(rating).State = EntityState.Detached;
            _context.Entry(rating).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Rating rating)
        {
            _context.Ratings.Update(rating);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CapacityAsync()
        {
            return await _context.Ratings.CountAsync();
        }

        public List<Rating> Where(Func<Rating, bool> predicate)
        {
            return _context.Ratings.Where(predicate).ToList();
        }
    }
}
