﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using GalleryStorage.StoragesInterfaces;
using Microsoft.EntityFrameworkCore;

namespace GalleryStorage.Storages
{
    public class PictureStorage : IStorage<Picture>
    {
        private readonly GalleryContext _context;

        public PictureStorage(GalleryContext context)
        {
            _context = context;
        }

        public async Task<Picture> GetAsync(Guid id)
        {
            return await _context.Pictures.Include(x => x.PictureTags).ThenInclude(x => x.Tag).Include(x => x.Comments).Include(x => x.Ratings).Include(x => x.Album).ThenInclude(x => x.User).SingleOrDefaultAsync(m => m.Id.Equals(id));
        }

        public async Task<ICollection<Picture>> GetAllAsync()
        {
            return await _context.Pictures.Include(x => x.PictureTags).ThenInclude(x => x.Tag).Include(x => x.Comments).Include(x => x.Ratings).Include(x => x.Album).ThenInclude(x => x.User).ToListAsync();
        }

        public async Task AddAsync(Picture picture)
        {
            await _context.Pictures.AddAsync(picture);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            Picture picture = await _context.Pictures.Include(x => x.PictureTags).ThenInclude(x => x.Tag).Include(x => x.Comments).Include(x => x.Ratings).SingleOrDefaultAsync(m => m.Id.Equals(id));
            if (picture == null)
            {
                return;
            }

            _context.Entry(picture).State = EntityState.Detached;
            _context.Entry(picture).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Picture picture)
        {
            if (picture == null)
            {
                return;
            }

            _context.Entry(picture).State = EntityState.Detached;
            _context.Entry(picture).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Picture picture)
        {
            _context.Pictures.Update(picture);
            await _context.SaveChangesAsync();
        }

        public async Task<int> CapacityAsync()
        {
            return await _context.Pictures.CountAsync();
        }

        public List<Picture> Where(Func<Picture, bool> predicate)
        {
            return _context.Pictures.Include(x => x.PictureTags).ThenInclude(x => x.Tag).Include(x => x.Comments).Include(x => x.Ratings).Where(predicate).ToList();
        }
    }
}
