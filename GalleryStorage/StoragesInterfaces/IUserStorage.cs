﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace GalleryStorage.StoragesInterfaces
{
    public interface IUserStorage<T> where T: IdentityUser<Guid>
    {
        Task<T> FindByIdAsync(Guid id);
        Task<T> FindByNameAsync(string name);
        Task<ICollection<T>> GetAllAsync();

        Task<IdentityResult> CreateAsync(T user, string password);
        Task<IdentityResult> UpdateAsync(T user);
        Task DeleteAsync(T user);

        Task<int> CapacityAsync();

        Task<IdentityResult> SetLockOutEnabledAsync(T user, bool enabled);
        Task<IdentityResult> SetLockoutEndDateAsync(T user, DateTimeOffset? lockoutEnd);
        Task<bool> IsLockedOutAsync(T user);
        Task<IdentityResult> ResetAccessFailedCountAsync(T user);
        Task<IdentityResult> ChangePasswordAsync(T user, string currentPassword, string newPassword);

        Task<IdentityResult> AddToRoleAsync(T user, string role);
        Task<IdentityResult> AddToRolesAsync(T user, IEnumerable<string> roles);
        Task<IdentityResult> RemoveFromRolesAsync(T user, IEnumerable<string> roles);
        Task<IList<string>> GetRolesAsync(T user);

        Task<string> GenerateEmailConfirmationTokenAsync(T user);
        Task<IdentityResult> ConfirmEmailAsync(T user, string token);
        Task<bool> IsEmailConfirmedAsync(T user);
        Task<string> GeneratePasswordResetTokenAsync(T user);
        Task<IdentityResult> ResetPasswordAsync(T user, string token, string newPassword);
    }
}
