﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace GalleryStorage.StoragesInterfaces
{
    public interface IRoleStorage<T> where T: IdentityRole<Guid>
    {
        IList<T> GetAll();
        Task<IdentityResult> CreateAsync(T role);
        Task<T> FindByNameAsync(string name);
        T FindById(Guid id);
        Task<IdentityResult> DeleteAsync(T role);
    }
}
