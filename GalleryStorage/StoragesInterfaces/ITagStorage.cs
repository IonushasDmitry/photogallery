﻿using System.Threading.Tasks;

namespace GalleryStorage.StoragesInterfaces
{
    public interface ITagStorage<T>: IStorage<T>
    {
        Task<T> GetByNameAsync(string name);
    }
}
