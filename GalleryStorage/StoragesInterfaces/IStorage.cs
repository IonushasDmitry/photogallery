﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GalleryStorage.StoragesInterfaces
{
    public interface IStorage<T>
    {
        Task<T> GetAsync(Guid id);
        Task<ICollection<T>> GetAllAsync();
        Task AddAsync(T item);
        Task DeleteAsync(Guid id);
        Task DeleteAsync(T item);
        Task UpdateAsync(T item);
        Task<int> CapacityAsync();
        List<T> Where(Func<T, bool> predicate);
    }
}
