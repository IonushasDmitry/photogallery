﻿using System.Collections.Generic;
namespace Gallery.Pager
{
    public interface IPager<T>
    {
        void SetRecordsOnPage(int recordsOnPage);
        void SetRecords(IList<T> records);
        int GetCountOfPages();
        IList<T> GetPage(int pageNumber);
        bool HasPage(int pageNumber);
    }
}
