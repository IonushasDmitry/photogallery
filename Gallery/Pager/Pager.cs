﻿using System;
using System.Collections.Generic;

namespace Gallery.Pager
{
    public class Pager<T>: IPager<T>
    {
        private int _recordsOnPage = 5;
        private IList<T> _records = new List<T>();

        public void SetRecordsOnPage(int recordsOnPage)
        {
            if (recordsOnPage <= 0)
            {
                throw new ArgumentException();
            }
            _recordsOnPage = recordsOnPage;
        }

        public void SetRecords(IList<T> records)
        {
            _records = records;
        }

        public int GetRecordsOnPage()
        {
            return _recordsOnPage;
        }

        public int GetCountOfPages()
        {
            int countOfRecords = _records.Count;
            int pages = countOfRecords / _recordsOnPage;
            if (countOfRecords % _recordsOnPage > 0)
            {
                pages++;
            }
            return pages;
        }

        public IList<T> GetPage(int pageNumber)
        {
            if (pageNumber > GetCountOfPages() || pageNumber < 1)
            {
                //throw new ArgumentException();
                return new List<T>();
            }

            int firstIndex = (pageNumber - 1) * _recordsOnPage;
            int lastIndex = pageNumber * _recordsOnPage;
            if (lastIndex > _records.Count)
            {
                lastIndex = _records.Count;
            }

            IList<T> page = new List<T>();
            for (int i = firstIndex; i < lastIndex; i++)
            {
                page.Add(_records[i]);
            }

            return page;
        }

        public bool HasPage(int pageNumber)
        {
            if (pageNumber > 0 && pageNumber <= GetCountOfPages())
            {
                return true;
            }
            return false;
        }
    }
}
