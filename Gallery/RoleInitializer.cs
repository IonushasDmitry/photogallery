﻿using System.Threading.Tasks;
using GalleryServices.Services;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Identity;

namespace Gallery
{
    public class RoleInitializer
    {
        public static async Task InitializeAsync(IUserServices<User> userServices, IRoleServices<Role> roleServices, string adminEmail, string adminPassword)
        {
            if (await roleServices.FindByNameAsync("admin") == null)
            {
                await roleServices.CreateAsync(new Role("admin"));
            }
            if (await roleServices.FindByNameAsync("user") == null)
            {
                await roleServices.CreateAsync(new Role("user"));
            }
            if (await userServices.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User
                {
                    FirstName = "Admin",
                    LastName = "Admin",
                    Pseudonym = "Admin",
                    Email = adminEmail,
                    UserName = adminEmail
                };
                IdentityResult result = await userServices.CreateAsync(admin, adminPassword);
                if (result.Succeeded)
                {
                    await userServices.AddToRoleAsync(admin, "admin");
                }
            }
        }
    }
}
