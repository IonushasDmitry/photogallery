﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GalleryStorage.Models;

namespace Gallery.Models.RolesManagingViewModels
{
    public class ChangeRoleViewModel
    {
        public Guid UserId { get; set; }

        [Display(Name = "Email")]
        public string UserEmail { get; set; }

        public List<Role> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }

        public ChangeRoleViewModel()
        {
            AllRoles = new List<Role>();
            UserRoles = new List<string>();
        }
    }
}
