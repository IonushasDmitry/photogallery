﻿using System;

namespace Gallery.Models.RolesManagingViewModels
{
    public class RoleViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
