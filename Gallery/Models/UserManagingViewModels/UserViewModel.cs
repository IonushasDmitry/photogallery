﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gallery.Models.PictureViewModels;
using GalleryStorage.Models;

namespace Gallery.Models.UserManagingViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Pseudonym { get; set; }

        public string Email { get; set; }
        public string Address { get; set; }

        [Display(Name = "Is locked out")]
        public bool IsLockedOut { get; set; }
        [Display(Name = "Block reason")]
        public string BlockReason { get; set; }

        public ICollection<string> UserRoles { get; set; }
        public virtual IList<Album> Albums { get; set; }
        public PictureSearchViewModel PictureSearchViewModel { get; set; }
    }
}
