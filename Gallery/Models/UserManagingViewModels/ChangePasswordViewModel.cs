﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.UserManagingViewModels
{
    public class ChangePasswordViewModel
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        [Display(Name = "Old password")]
        public string OldPassword { get; set; }
        [Display(Name = "New password")]
        public string NewPassword { get; set; }
    }
}
