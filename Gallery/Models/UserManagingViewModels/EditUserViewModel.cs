﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.UserManagingViewModels
{
    public class EditUserViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [RegularExpression("[A-Za-z ]*", ErrorMessage = "First name must consist only from letters and whitespaces")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [RegularExpression("[A-Za-z ]*", ErrorMessage = "Last name must consist only from letters and whitespaces")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Pseudonym")]
        public string Pseudonym { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Address")]
        [StringLength(50, ErrorMessage = "The address can not be longer than 50 characters")]
        public string Address { get; set; }
    }
}
