﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.UserManagingViewModels
{
    public class LockUserViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pseudonym { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        [Display(Name = "Block reason")]
        public string BlockReason { get; set; }
    }
}
