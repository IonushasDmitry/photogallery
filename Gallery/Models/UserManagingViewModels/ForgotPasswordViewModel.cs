﻿using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.UserManagingViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
    }
}
