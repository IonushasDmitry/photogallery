﻿using GalleryStorage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.PictureViewModels
{
    public class PictureViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public string Path { get; set; }

        public string RelativePath { get; set; }
        public string ThumbnailRelativePath { get; set; }

        public DateTime PostDateTime { get; set; }
        public Guid UserId { get; set; }
        public Album Album { get; set; }


        public virtual IList<Rating> Ratings { get; set; }
        public virtual IList<CommentViewModel> Comments { get; set; }
        public virtual IList<string> TagNames { get; set; } = new List<string>();
    }
}
