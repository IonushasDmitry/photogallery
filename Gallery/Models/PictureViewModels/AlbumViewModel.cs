﻿using GalleryStorage.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.PictureViewModels
{
    public class AlbumViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public User User { get; set; }

        public PictureSearchViewModel PictureSearchViewModel { get; set; }
    }
}
