﻿using System;
using GalleryStorage.Models;

namespace Gallery.Models.PictureViewModels
{
    public class RatingViewModel
    {
        public Guid Id { get; set; }

        public Picture Picture { get; set; }
        public User User { get; set; }
    }
}
