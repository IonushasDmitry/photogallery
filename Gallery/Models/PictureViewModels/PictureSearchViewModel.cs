﻿using System.Collections.Generic;
using Gallery.Sorting;

namespace Gallery.Models.PictureViewModels
{
    public class PictureSearchViewModel
    {
        public IEnumerable<PictureViewModel> Pictures { get; set; }
        public PictureSortState SortOrder { get; set; }
        public int PageNumber { get; set; }
        public int CountOfPages { get; set; }
    }
}
