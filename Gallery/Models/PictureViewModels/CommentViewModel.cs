﻿using System;
using GalleryStorage.Models;

namespace Gallery.Models.PictureViewModels
{
    public class CommentViewModel
    {
        public Guid Id { get; set; }
        public string Context { get; set; }
        public DateTime PostDateTime { get; set; }
        public string UserPseudonym { get; set; }

        public Guid UserId { get; set; }
        public Guid PictureId { get; set; }

        public User User { get; set; }
        public Picture Picture { get; set; }
    }
}
