﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.PictureViewModels
{
    public class ChangeAlbumViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Display(Name = "New name")]
        [Required]
        public string Name { get; set; }
    }
}
