﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Gallery.Models.PictureViewModels
{
    public class AddingPictureViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }

        [Display(Name = "Enter tags separated by commas")]
        public string Tags { get; set; }

        [Required(ErrorMessage = "File not choosen")]
        public IFormFile Picture { get; set; }

        [Required]
        public Guid AlbumId { get; set; }
    }
}
