﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gallery.Models.RolesManagingViewModels;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Gallery.Controllers
{
    [Authorize(Roles = "admin")]
    public class RolesController : Controller
    {
        private readonly IUserServices<User> _userServices;
        private readonly IRoleServices<Role> _roleServices;

        public RolesController(IUserServices<User> userServices, IRoleServices<Role> roleServices)
        {
            _userServices = userServices;
            _roleServices = roleServices;
        }
        
        public async Task<IActionResult> Edit(Guid id)
        {
            User user = await _userServices.FindByIdAsync(id);
            if (user != null)
            {
                var userRoles = await _userServices.GetRolesAsync(user);
                var allRoles = _roleServices.GetAll();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles.ToList()
                };
                return View(model);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, List<string> roles)
        {
            User user = await _userServices.FindByIdAsync(id);
            if (user != null)
            {
                var userRoles = await _userServices.GetRolesAsync(user);
                var addedRoles = roles.Except(userRoles);
                var removedRoles = userRoles.Except(roles);

                await _userServices.AddToRolesAsync(user, addedRoles);
                await _userServices.RemoveFromRolesAsync(user, removedRoles);
                return RedirectToAction("UsersList", "Users");
            }

            return NotFound();
        }
    }
}