﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Gallery.Models.PictureViewModels;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Mvc;

namespace Gallery.Controllers
{
    public class PictureController : BaseController
    {
        private readonly IAlbumServices<Album> _albumServices;
        private readonly IPictureServices<Picture> _pictureServices;
        private readonly ITagServices<Tag> _tagServices;
        private readonly IEmailServices _emailServices;

        public PictureController(IAlbumServices<Album> albumServices, IPictureServices<Picture> pictureServices, 
            IUserServices<User> userServices, ITagServices<Tag> tagServices, IEmailServices emailServices)
                : base(userServices)
        {
            _albumServices = albumServices;
            _pictureServices = pictureServices;
            _userServices = userServices;
            _tagServices = tagServices;
            _emailServices = emailServices;
        }


        public IActionResult AddPicture(Guid albumId)
        {
            AddingPictureViewModel model = new AddingPictureViewModel() { AlbumId = albumId };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddPicture(AddingPictureViewModel pictureViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(pictureViewModel);
            }

            List<string> tagNames = pictureViewModel.Tags.Split(',').ToList();
            for (int i = 0; i < tagNames.Count; i++)
            {
                tagNames[i] = tagNames[i].Trim();
            }

            List<Tag> tags = new List<Tag>();
            foreach (var tagName in tagNames)
            {
                Tag tag = (await _tagServices.GetByNameAsync(tagName));
                if (tag == null)
                {
                    tag = new Tag { Name = tagName };
                    await _tagServices.AddAsync(tag);
                }
                tags.Add(tag);
            }

            Album album = await _albumServices.GetAsync(pictureViewModel.AlbumId);

            Picture picture = new Picture
            {
                Name = pictureViewModel.Name,
                Description = pictureViewModel.Description,
                FileName = pictureViewModel.Picture.FileName,
                PostDateTime = DateTime.Now,
                UserId = album.UserId,
                Album = album
            };
            picture.FileName = picture.Id + Path.GetExtension(pictureViewModel.Picture.FileName);
            await _pictureServices.AddAsync(picture);

            foreach (var tag in tags)
            {
                picture.PictureTags.Add(new PictureTag { PictureId = picture.Id, TagId = tag.Id});
            }
            await _pictureServices.UpdateAsync(picture);

            try
            {
                using (var fileStream = new FileStream(_pictureServices.GetFullPath(picture), FileMode.Create))
                {
                    await pictureViewModel.Picture.CopyToAsync(fileStream);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return RedirectToAction("Album", "Album", new { albumId = album.Id });
            }

            _pictureServices.CreateThumbnail(picture);

            return RedirectToAction("Album", "Album", new { albumId = album.Id });
        }

        [HttpPost]
        public async Task<IActionResult> DeletePicture(Guid pictureId)
        {
            Album album = (await _pictureServices.GetAsync(pictureId)).Album;
            if (!(await IsAdminOrCurrentUser(album.User)))
            {
                return StatusCode(401);
            }
            await _pictureServices.DeleteAsync(await _pictureServices.GetAsync(pictureId));

            return RedirectToAction("Album", "Album", new { albumId = album.Id });
        }

        [HttpGet]
        public async Task<IActionResult> DeletePictureForReason(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
            pictureViewModel.RelativePath = _pictureServices.GetRelativePath(picture);

            return View(pictureViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeletePictureForReason(Guid pictureId, string reason)
        {
            if (!User.IsInRole("admin"))
            {
                return StatusCode(401);
            }

            Picture picture = await _pictureServices.GetAsync(pictureId);
            User user = await _userServices.FindByIdAsync(picture.UserId);
            await _emailServices.SendEmailAsync(user.Email, "Message from Gallery administration",
                $"Your work \"{picture.Name}\" was deleted for reason: {reason}");

            Album album = (await _pictureServices.GetAsync(pictureId)).Album;
            await _pictureServices.DeleteAsync(picture);

            return RedirectToAction("Album", "Album", new { albumId = album.Id });
        }

        public async Task<IActionResult> ViewPictureInFullSize(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
            pictureViewModel.RelativePath = _pictureServices.GetRelativePath(picture);
            foreach (var commentViewModel in pictureViewModel.Comments)
            {
                User user = await _userServices.FindByIdAsync(commentViewModel.UserId);
                if (user != null)
                {
                    commentViewModel.UserPseudonym = user.Pseudonym;
                }
                else
                {
                    commentViewModel.UserPseudonym = "Deleted user";
                }
            }

            ViewBag.UserIsAuthenticated = User.Identity.IsAuthenticated;
            ViewBag.IsAdmin = CurrentUserIsAdmin();
            ViewBag.CurrentUserId = await CurrentUserId();
            ViewBag.OwnerId = picture.UserId;
            ViewBag.OwnerPseudonym = (await _userServices.FindByIdAsync(picture.UserId)).Pseudonym;

            Album album = await _albumServices.GetAsync(picture.AlbumId);
            var pictures = album.Pictures.OrderByDescending(x => x.PostDateTime).ToList();
            var indexOfPicture = pictures.IndexOf(picture);

            ViewBag.NextPictureId = indexOfPicture == 0 ? pictures.Last().Id : pictures.ElementAt(indexOfPicture - 1).Id;
            ViewBag.PreviousPictureId = indexOfPicture == (pictures.Count() - 1) ? pictures.First().Id : pictures.ElementAt(indexOfPicture + 1).Id;
            return View(pictureViewModel);
        }

        public async Task<JsonResult> GetPictureRating(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            return Json(new { value = picture.Ratings.Count });
        }
    }
}