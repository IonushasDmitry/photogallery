﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Mvc;

namespace Gallery.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IUserServices<User> _userServices { get; set; }
        
        protected BaseController(IUserServices<User> userServices)
        {
            _userServices = userServices;
        }

        protected async Task<bool> IsAdminOrCurrentUser(User user)
        {
            if (CurrentUserIsAdmin() || (await IsCurrentUser(user)))
            {
                return true;
            }

            return false;
        }

        protected bool CurrentUserIsAdmin()
        {
            return User.IsInRole("admin");
        }

        protected async Task<bool> IsCurrentUser(User user)
        {
            if ((await CurrentUserId()) == user.Id)
            {
                return true;
            }
            return false;
        }

        protected async Task<Guid> CurrentUserId()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return new Guid();
            }
            return (await _userServices.FindByNameAsync(User.Identity.Name)).Id;
        }
    }
}