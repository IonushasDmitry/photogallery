﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Gallery.Models.PictureViewModels;
using Gallery.Sorting;
using Gallery.Pager;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Gallery.Controllers
{
    public class SearchController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserServices<User> _userServices;
        private readonly IPictureServices<Picture> _pictureServices;
        private List<Picture> FoundPictures { get; set; }
        private readonly int _picturesOnPage;

        public SearchController(IConfiguration configuration, IUserServices<User> userServices, IPictureServices<Picture> pictureServices)
        {
            _configuration = configuration;
            _picturesOnPage = Int32.TryParse(_configuration["PicturesOnPage:Default"], out _picturesOnPage) ? Int32.Parse(_configuration["PicturesOnPage:Default"]) : 16;
            _userServices = userServices;
            _pictureServices = pictureServices;
        }


        public async Task<IActionResult> GetFoundPicturesPage(string searchExpression, int? page, PictureSortState? sortOrder)
        {
            ViewBag.SearchExpression = searchExpression;
            await SearchInAllPictures(searchExpression);
            if (FoundPictures.Count == 0)
            {
                PictureSearchViewModel emptyModel = new PictureSearchViewModel
                {
                    Pictures = new List<PictureViewModel>(),
                    SortOrder = PictureSortState.PostDateTimeDesc,
                    PageNumber = 1,
                    CountOfPages = 1
                };

                return PartialView(emptyModel);
            }

            int pageNumber = page ?? 1;
            if (pageNumber == 0)
            {
                pageNumber = 1;
            }
            PictureSortState order = sortOrder ?? PictureSortState.PostDateTimeDesc;

            PictureSorter sorter = new PictureSorter
            {
                Pictures = FoundPictures.AsQueryable(),
                SortOrder = order
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = new List<PictureViewModel>();
            foreach (var picture in pager.GetPage(pageNumber))
            {
                PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
                pictureViewModel.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
                pictureViewModels.Add(pictureViewModel);
            }

            PictureSearchViewModel model = new PictureSearchViewModel
            {
                Pictures = pictureViewModels,
                SortOrder = order,
                PageNumber = pageNumber,
                CountOfPages = pager.GetCountOfPages()
            };

            return PartialView(model);
        }

        private async Task<IEnumerable<Picture>> SearchByUserName(string userName)
        {
            List<Picture> usersPictures = new List<Picture>();
            IEnumerable<User> users = (await _userServices.GetAllAsync()).Where(x => x.Pseudonym.ToLower().IndexOf(userName.ToLower(), StringComparison.CurrentCulture) != -1);
            foreach (User user in users)
            {
                foreach (Album album in user.Albums)
                {
                    usersPictures.AddRange(album.Pictures);
                }
            }

            return usersPictures;
        }

        private IEnumerable<Picture> SearchByPictureName(ICollection<Picture> pictures, string searchExpression)
        {
            string pictureName = searchExpression.ToLower();
            return pictures.Where(x => x.Name.ToLower().IndexOf(pictureName, StringComparison.CurrentCulture) != -1);
        }

        private IEnumerable<Picture> SearchByDescription(ICollection<Picture> pictures, string searchExpression)
        {
            string description = searchExpression.ToLower();
            return pictures.Where(x => x.Description.ToLower().IndexOf(description, StringComparison.CurrentCulture) != -1);
        }

        private IEnumerable<Picture> SearchByTag(ICollection<Picture> pictures, string searchExpression)
        {
            string tagName = searchExpression.ToLower();
            List<Picture> picturesList = new List<Picture>();
            foreach (var picture in pictures)
            {
                var tags = picture.PictureTags.Select(x => x.Tag);
                foreach (var tag in tags)
                {
                    if (tag.Name.ToLower() == tagName)
                    {
                        picturesList.Add(picture);
                    }
                }
            }

            return picturesList;

        }

        public async Task SearchInAllPictures(string searchExpression)
        {
            if (string.IsNullOrEmpty(searchExpression))
            {
                FoundPictures = new List<Picture>();
                return;
            }

            ICollection<Picture> allPictures = await _pictureServices.GetAllAsync();
            List<Picture> foundPictures = SearchByTag(allPictures, searchExpression).ToList();
            foundPictures.AddRange(await SearchByUserName(searchExpression));
            foundPictures.AddRange(SearchByPictureName(allPictures, searchExpression));
            foundPictures.AddRange(SearchByDescription(allPictures, searchExpression));

            FoundPictures = foundPictures.Distinct().ToList();
        }

        [HttpPost]
        public async Task<IActionResult> Search(string searchExpression)
        {
            ViewBag.SearchExpression = searchExpression;
            await SearchInAllPictures(searchExpression);
            if (FoundPictures.Count == 0)
            {
                PictureSearchViewModel emptyModel = new PictureSearchViewModel
                {
                    Pictures = new List<PictureViewModel>(),
                    SortOrder = PictureSortState.PostDateTimeDesc,
                    PageNumber = 1,
                    CountOfPages = 1
                };

                return PartialView(emptyModel);
            }

            int pageNumber = 1;
            PictureSortState order = PictureSortState.PostDateTimeDesc;

            PictureSorter sorter = new PictureSorter
            {
                Pictures = FoundPictures.AsQueryable(),
                SortOrder = order
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = new List<PictureViewModel>();
            foreach (var picture in pager.GetPage(pageNumber))
            {
                PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
                pictureViewModel.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
                pictureViewModels.Add(pictureViewModel);
            }

            PictureSearchViewModel model = new PictureSearchViewModel
            {
                Pictures = pictureViewModels,
                SortOrder = order,
                PageNumber = pageNumber,
                CountOfPages = pager.GetCountOfPages()
            };

            return PartialView(model);
        }

        public IActionResult Index(string searchExpression = null)
        {
            ViewBag.SearchExpression = searchExpression;
            return View();
        }
    }
}