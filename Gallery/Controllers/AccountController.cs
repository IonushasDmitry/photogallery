﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Gallery.Models.UserManagingViewModels;
using GalleryServices.ServicesInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Gallery.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserServices<User> _userServices;
        private readonly ISignInServices<User> _signInServices;
        private readonly IEmailServices _emailServices;
        private readonly UserManager<User> _userManager;
        private const int DaysBetweenExpiryOfPassword = 182;

        public AccountController(IUserServices<User> userServices, ISignInServices<User> signInServices, IEmailServices emailServices, UserManager<User> userManager)
        {
            _userServices = userServices;
            _signInServices = signInServices;
            _emailServices = emailServices;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(UserRegistrationViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                User tmpUser = (await _userServices.GetAllAsync()).SingleOrDefault(x => x.Pseudonym == model.Pseudonym);
                if (tmpUser != null)
                {
                    ModelState.AddModelError(string.Empty, "Pseudonym is already taken");
                    return View(model);
                }
                User user = Mapper.Map<UserRegistrationViewModel, User>(model);
                if (string.IsNullOrEmpty(user.Pseudonym))
                {
                    user.Pseudonym = $"{user.FirstName} {user.LastName}";
                }
                user.DateOfPasswordChange = DateTime.Now;

                var result = await _userServices.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var code = await _userServices.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        new { userId = user.Id, code },
                        protocol: HttpContext.Request.Scheme);
                    await _emailServices.SendEmailAsync(model.Email, "Confirm your account",
                        $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var user = await _userServices.FindByIdAsync(Guid.Parse(userId));
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userServices.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                List<string> userRole = new List<string>
                {
                    "user"
                };
                await _userServices.AddToRolesAsync(user, userRole);
                await _signInServices.SignInAsync(user, false);
                return RedirectToAction("Index", "Home");
            }
            return View("Error");
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInServices.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, true);
                if (result.Succeeded)
                {
                    User user = await _userServices.FindByNameAsync(model.Email);
                    if ((DateTime.Now - user.DateOfPasswordChange).TotalDays > DaysBetweenExpiryOfPassword)
                    {
                        return RedirectToAction("ChangePassword", "Users",
                            new
                            {
                                id = user.Id,
                                message =
                                "You changed your password more than half a year ago. Please, change your password."
                            });
                    }
                    // check if the URL belongs to the application.
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                if (result.IsLockedOut)
                {
                    User user = await _userServices.FindByNameAsync(model.Email);
                    ModelState.AddModelError("", $"User {model.Email} is blocked. Reason: {user.BlockReason}");
                }
                else
                {
                    ModelState.AddModelError("", "Wrong email and/or password");
                }
            }
            return View(model);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInServices.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View("ForgotPassword");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userServices.FindByNameAsync(model.Email);
                if (user == null || !(await _userServices.IsEmailConfirmedAsync(user)))
                {
                    return View("ForgotPasswordConfirmation");
                }

                var code = await _userServices.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code }, protocol: HttpContext.Request.Scheme);
                await _emailServices.SendEmailAsync(model.Email, "Reset Password",
                    $"To reset password follow link: <a href='{callbackUrl}'>link</a>");

                user.PasswordHash = null;
                await _userServices.UpdateAsync(user);

                return View("ForgotPasswordConfirmation");
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userServices.FindByNameAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await _userServices.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }
    }
}