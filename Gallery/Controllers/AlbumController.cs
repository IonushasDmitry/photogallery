﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Gallery.Models.PictureViewModels;
using Gallery.Sorting;
using Gallery.Pager;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Gallery.Controllers
{
    public class AlbumController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IAlbumServices<Album> _albumServices;
        private readonly IPictureServices<Picture> _pictureServices;
        private readonly int _picturesOnPage;

        public AlbumController(IConfiguration configuration, IUserServices<User> userServices, IAlbumServices<Album> albumServices, IPictureServices<Picture> pictureServices)
            :base(userServices)
        {
            _configuration = configuration;
            _picturesOnPage = Int32.TryParse(_configuration["PicturesOnPage:Default"], out _picturesOnPage) ? Int32.Parse(_configuration["PicturesOnPage:Default"]) : 16;
            _userServices = userServices;
            _albumServices = albumServices;
            _pictureServices = pictureServices;
        }


        public async Task<IActionResult> GetAlbumPicturesPage(Guid albumId, int? page, PictureSortState? sortOrder)
        {
            IList<Picture> albumPictures = (await _albumServices.GetAsync(albumId)).Pictures;

            int pageNumber = page ?? 1;
            if (pageNumber == 0)
            {
                pageNumber = 1;
            }
            PictureSortState order = sortOrder ?? PictureSortState.PostDateTimeDesc;

            PictureSorter sorter = new PictureSorter
            {
                Pictures = albumPictures.AsQueryable(),
                SortOrder = order
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = new List<PictureViewModel>();
            foreach (var picture in pager.GetPage(pageNumber))
            {
                PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
                pictureViewModel.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
                pictureViewModels.Add(pictureViewModel);
            }

            PictureSearchViewModel model = new PictureSearchViewModel
            {
                Pictures = pictureViewModels,
                SortOrder = order,
                PageNumber = pageNumber,
                CountOfPages = pager.GetCountOfPages()
            };
            ViewBag.AlbumId = albumId;

            return PartialView(model);
        }


        public async Task<IActionResult> Albums(Guid userId)
        {
            User user = await _userServices.FindByIdAsync(userId);
            List<AlbumViewModel> albums = new List<AlbumViewModel>();

            if (user.Albums != null)
            {
                foreach (var album in user.Albums)
                {
                    albums.Add(Mapper.Map<Album, AlbumViewModel>(album));
                }
            }

            return View(albums);
        }

        [Authorize]
        public async Task<IActionResult> MyAlbums()
        {
            User user = await _userServices.FindByNameAsync(User.Identity.Name);
            return RedirectToAction("Albums", new {userId = user.Id});
        }

        public async Task<IActionResult> Album(Guid albumId)
        {
            Album album = await _albumServices.GetAsync(albumId);
            foreach (var picture in album.Pictures)
            {
                if (!System.IO.File.Exists(_pictureServices.GetThumbnailFullPath(picture)))
                {
                    _pictureServices.CreateThumbnail(picture);
                }
            }

            AlbumViewModel albumViewModel = Mapper.Map<Album, AlbumViewModel>(album);

            ViewBag.UserIsAuthenticated = User.Identity.IsAuthenticated;
            ViewBag.IsAdmin = CurrentUserIsAdmin();
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUserId = await CurrentUserId();
            }
            else
            {
                ViewBag.CurrentUserId = new Guid();
            }
            ViewBag.AlbumId = albumId;



            IList<Picture> albumPictures = (await _albumServices.GetAsync(albumId)).Pictures;

            int pageNumber = 1;
            PictureSortState sortOrder = PictureSortState.PostDateTimeDesc;
            PictureSorter sorter = new PictureSorter
            {
                Pictures = albumPictures.AsQueryable(),
                SortOrder = sortOrder
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = Mapper.Map<IList<Picture>, List<PictureViewModel>>(pager.GetPage(pageNumber));
            foreach (var picture in pictureViewModels)
            {
                picture.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
            }

            PictureSearchViewModel pictureSearchViewModel = new PictureSearchViewModel
            {
                PageNumber = 1,
                Pictures = pictureViewModels,
                SortOrder = sortOrder,
                CountOfPages = pager.GetCountOfPages()
            };

            albumViewModel.PictureSearchViewModel = pictureSearchViewModel;

            return View(albumViewModel);
        }

        public IActionResult AddAlbum()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddAlbum(AlbumViewModel albumViewModel)
        {
            User user = await _userServices.FindByNameAsync(User.Identity.Name);
            Album album = new Album()
            {
                User = user,
                Name = albumViewModel.Name
            };

            await _albumServices.AddAsync(album);

            return RedirectToAction("Albums", new { userId = user.Id });
        }

        public async Task<IActionResult> DeleteAlbum(Guid albumId)
        {
            Album album = await _albumServices.GetAsync(albumId);
            Guid userId = album.User.Id;
            if (!(await IsAdminOrCurrentUser(album.User)))
            {
                return StatusCode(401);
            }
            await _albumServices.DeleteAsync(album);

            return RedirectToAction("Albums", new { userId });
        }

        public async Task<IActionResult> ChangeAlbum(Guid id)
        {
            Album album = await _albumServices.GetAsync(id);
            ChangeAlbumViewModel changeAlbumModel = Mapper.Map<Album, ChangeAlbumViewModel>(album);
            return View(changeAlbumModel);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeAlbum(ChangeAlbumViewModel model)
        {
            Album album = await _albumServices.GetAsync(model.Id);
            album.Name = model.Name;
            await _albumServices.UpdateAsync(album);
            return RedirectToAction("Album", new {albumId = model.Id});
        }
    }
}