﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Gallery.Controllers
{
    [Authorize]
    public class RatingController : Controller
    {
        private readonly IServices<Rating> _ratingServices;
        private readonly IPictureServices<Picture> _pictureServices;
        private readonly IUserServices<User> _userServices;

        public RatingController(IServices<Rating> ratingServices, IPictureServices<Picture> pictureServices, IUserServices<User> userServices)
        {
            _ratingServices = ratingServices;
            _pictureServices = pictureServices;
            _userServices = userServices;
        }

        public async Task<JsonResult> GetRating(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            return Json(new { value = picture.Ratings.Count });
        }

        [HttpPost]
        public async Task<JsonResult> ChangeRating(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            User user = await _userServices.FindByNameAsync(User.Identity.Name);
            Rating rating = picture.Ratings.SingleOrDefault(x => x.UserId == user.Id);

            if (rating == null)
            {
                return await AddRating(pictureId);
            }

            return await DeleteRating(pictureId);
        }

        [HttpPost]
        public async Task<JsonResult> AddRating(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            User user = await _userServices.FindByNameAsync(User.Identity.Name);
            Rating rating = new Rating
            {
                UserId = user.Id,
                Picture = picture
            };
            await _ratingServices.AddAsync(rating);

            return Json(new {value = picture.Ratings.Count });
        }

        [HttpPost]
        public async Task<JsonResult> DeleteRating(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            User user = await _userServices.FindByNameAsync(User.Identity.Name);
            Rating rating = picture.Ratings.SingleOrDefault(x => x.UserId == user.Id);
            await _ratingServices.DeleteAsync(rating);

            //return picture.Ratings.Count.ToString();
            //return $"<div>{picture.Ratings.Count} likes</div>";
            return Json(new { value = picture.Ratings.Count });
        }
    }
}