﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gallery.Models;
using GalleryStorage.Models;
using AutoMapper;
using Gallery.Models.PictureViewModels;
using Gallery.Models.UserManagingViewModels;
using Gallery.Pager;
using Gallery.Sorting;
using GalleryServices.ServicesInterfaces;
using Microsoft.Extensions.Configuration;
using System;

namespace Gallery.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserServices<User> _userServices;
        private readonly IPictureServices<Picture> _pictureServices;
        private readonly int _picturesOnPage;

        public HomeController(IConfiguration configuration, IUserServices<User> userServices, IPictureServices<Picture> pictureServices)
        {
            _configuration = configuration;
            _picturesOnPage = Int32.TryParse(_configuration["PicturesOnPage:Default"], out _picturesOnPage) ? Int32.Parse(_configuration["PicturesOnPage:Default"]) : 16;
            _userServices = userServices;
            _pictureServices = pictureServices;
        }

        public IActionResult LoginPage()
        {
            return View();
        }

        public async Task<IActionResult> Index()
        {
            int pageNumber = 1;
            PictureSortState sortOrder = PictureSortState.PostDateTimeDesc;
            PictureSorter sorter = new PictureSorter
            {
                Pictures = (await _pictureServices.GetAllAsync()).AsQueryable(),
                SortOrder = sortOrder
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = Mapper.Map<IList<Picture>, List<PictureViewModel>>(pager.GetPage(pageNumber));
            foreach (var picture in pictureViewModels)
            {
                picture.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
            }

            PictureSearchViewModel model = new PictureSearchViewModel
            {
                PageNumber = 1,
                Pictures = pictureViewModels,
                SortOrder = sortOrder,
                CountOfPages = pager.GetCountOfPages()
            };

            return View(model);
        }

        public async Task<IActionResult> GetPicturesPage(int? page, PictureSortState? sortOrder)
        {
            int pageNumber = page ?? 1;
            if (pageNumber == 0)
            {
                pageNumber = 1;
            }
            PictureSortState order = sortOrder ?? PictureSortState.PostDateTimeDesc;

            PictureSorter sorter = new PictureSorter
            {
                Pictures = (await _pictureServices.GetAllAsync()).AsQueryable(),
                SortOrder = order
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = new List<PictureViewModel>();
            foreach (var picture in pager.GetPage(pageNumber))
            {
                PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
                pictureViewModel.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
                pictureViewModels.Add(pictureViewModel);
            }

            PictureSearchViewModel model = new PictureSearchViewModel
            {
                Pictures = pictureViewModels,
                SortOrder = order,
                PageNumber = pageNumber,
                CountOfPages = pager.GetCountOfPages()
            };

            return PartialView(model);
        }

        public async Task<IActionResult> PersonalArea()
        {
            User user = await _userServices.FindByNameAsync(User.Identity.Name);
            UserViewModel model = Mapper.Map<User, UserViewModel>(user);
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}



