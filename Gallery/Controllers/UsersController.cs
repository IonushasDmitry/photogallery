﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Gallery.Models.PictureViewModels;
using GalleryStorage.Models;
using Gallery.Models.UserManagingViewModels;
using Gallery.Sorting;
using Gallery.Pager;
using GalleryServices.ServicesInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace Gallery.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly ISignInServices<User> _signInServices;
        private readonly IPictureServices<Picture> _pictureServices;
        private readonly int _picturesOnPage;

        public UsersController(IConfiguration configuration, IUserServices<User> userServices, ISignInServices<User> signInServices, IPictureServices<Picture> pictureServices)
            : base(userServices)
        {
            _configuration = configuration;
            _picturesOnPage = Int32.TryParse(_configuration["PicturesOnPage:Default"], out _picturesOnPage) ? Int32.Parse(_configuration["PicturesOnPage:Default"]) : 16;
            _userServices = userServices;
            _signInServices = signInServices;
            _pictureServices = pictureServices;
        }


        public async Task<IActionResult> GetUserPicturesPage(Guid userId, int? page, PictureSortState? sortOrder)
        {
            User user = await _userServices.FindByIdAsync(userId);
            List<Picture> userPictures = new List<Picture>();
            foreach (var album in user.Albums)
            {
                userPictures.AddRange(album.Pictures);
            }

            int pageNumber = page ?? 1;
            if (pageNumber == 0)
            {
                pageNumber = 1;
            }
            PictureSortState order = sortOrder ?? PictureSortState.PostDateTimeDesc;

            PictureSorter sorter = new PictureSorter
            {
                Pictures = userPictures.AsQueryable(),
                SortOrder = order
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            var picturesForPager = sorter.Sort().ToList();
            pager.SetRecords(picturesForPager);

            List<PictureViewModel> pictureViewModels = new List<PictureViewModel>();
            foreach (var picture in pager.GetPage(pageNumber))
            {
                PictureViewModel pictureViewModel = Mapper.Map<Picture, PictureViewModel>(picture);
                pictureViewModel.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
                pictureViewModels.Add(pictureViewModel);
            }

            PictureSearchViewModel model = new PictureSearchViewModel
            {
                Pictures = pictureViewModels,
                SortOrder = order,
                PageNumber = pageNumber,
                CountOfPages = pager.GetCountOfPages()
            };
            ViewBag.UserId = userId;

            return PartialView(model);
        }

        public async Task<IActionResult> UsersList()
        {
            if (User.IsInRole("admin"))
            {
                return RedirectToAction("UsersListForAdmin");
            }

            ICollection<User> users = await _userServices.GetAllAsync();
            List<UserViewModel> usersList = new List<UserViewModel>();

            foreach(var user in users)
            {
                UserViewModel model = Mapper.Map<User, UserViewModel>(user);
                usersList.Add(model);
            }

            return View(usersList);
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UsersListForAdmin()
        {
            ICollection<User> users = await _userServices.GetAllAsync();
            List<UserViewModel> usersList = new List<UserViewModel>();

            foreach (var user in users)
            {
                UserViewModel model = Mapper.Map<User, UserViewModel>(user);
                model.IsLockedOut = await _userServices.IsLockedOutAsync(user);
                usersList.Add(model);
            }

            return View(usersList);
        }

        public async Task<IActionResult> Profile(Guid id)
        {
            User user = await _userServices.FindByIdAsync(id);
            UserViewModel userViewModel = Mapper.Map<User, UserViewModel>(user);

            List<Picture> userPictures = new List<Picture>();
            foreach (var album in userViewModel.Albums)
            {
                userPictures.AddRange(album.Pictures);
            }

            int pageNumber = 1;
            PictureSortState sortOrder = PictureSortState.PostDateTimeDesc;
            PictureSorter sorter = new PictureSorter
            {
                Pictures = userPictures.AsQueryable(),
                SortOrder = sortOrder
            };

            Pager<Picture> pager = new Pager<Picture>();
            pager.SetRecordsOnPage(_picturesOnPage);
            pager.SetRecords(sorter.Sort().ToList());

            List<PictureViewModel> pictureViewModels = Mapper.Map<IList<Picture>, List<PictureViewModel>>(pager.GetPage(pageNumber));
            foreach (var picture in pictureViewModels)
            {
                picture.ThumbnailRelativePath = _pictureServices.GetThumbnailRelativePath(await _pictureServices.GetAsync(picture.Id));
            }

            PictureSearchViewModel pictureSearchViewModel = new PictureSearchViewModel
            {
                PageNumber = 1,
                Pictures = pictureViewModels,
                SortOrder = sortOrder,
                CountOfPages = pager.GetCountOfPages()
            };

            userViewModel.PictureSearchViewModel = pictureSearchViewModel;
            ViewBag.UserId = id;

            return View(userViewModel);
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            User user = await _userServices.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            if (!(await IsAdminOrCurrentUser(user)))
            {
                return StatusCode(401);
            }
            EditUserViewModel editModel = Mapper.Map<User, EditUserViewModel>(user);
            return View(editModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userServices.FindByIdAsync(model.Id);
                if (!(await IsAdminOrCurrentUser(user)))
                {
                    return StatusCode(401);
                }
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Pseudonym = model.Pseudonym;
                user.Email = model.Email;
                user.Address = model.Address;
                user.UserName = model.Email;

                var result = await _userServices.UpdateAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("UsersList");
                }
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            User user = await _userServices.FindByIdAsync(id);
            if (!await IsAdminOrCurrentUser(user))
            {
                return StatusCode(401);
            }
            if (user != null)
            {
                await _signInServices.SignOutAsync();
                await _userServices.DeleteAsync(user);
            }
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> LockUser(Guid id)
        {
            User user = await _userServices.FindByIdAsync(id);
            LockUserViewModel lockUserViewModel = Mapper.Map<User, LockUserViewModel>(user);
            return View(lockUserViewModel);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> LockUser(LockUserViewModel lockUserViewModel)
        {
            User user = await _userServices.FindByIdAsync(lockUserViewModel.Id);
            if (user != null)
            {
                var result = await _userServices.SetLockOutEnabledAsync(user, true);
                if (result.Succeeded)
                {
                    user.BlockReason = lockUserViewModel.BlockReason;
                    await _userServices.UpdateAsync(user);
                    await _userServices.SetLockOutEndDateAsync(user, DateTimeOffset.MaxValue);
                }
            }
            return RedirectToAction("UsersList");
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> UnlockUser(Guid id)
        {
            User user = await _userServices.FindByIdAsync(id);
            if (user != null)
            {
                user.BlockReason = string.Empty;
                await _userServices.UpdateAsync(user);
                await _userServices.ResetAccessFailedCountAsync(user);
                await _userServices.SetLockOutEndDateAsync(user, DateTimeOffset.MinValue);
            }
            return RedirectToAction("UsersList");
        }

        public async Task<IActionResult> ChangePassword(Guid id, string message = null)
        {
            User user = await _userServices.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            if (!(await IsCurrentUser(user)))
            {
                return StatusCode(401);
            }
            ChangePasswordViewModel model = new ChangePasswordViewModel { Id = user.Id, Email = user.Email };
            ViewBag.Message = message;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (model.OldPassword == model.NewPassword)
            {
                ModelState.AddModelError(string.Empty, "New password cannot be the same as old password");
            }
            if (model.Email == model.NewPassword)
            {
                ModelState.AddModelError(string.Empty, "New password cannot be the same as email");
            }
            if (ModelState.IsValid)
            {
                User user = await _userServices.FindByIdAsync(model.Id);
                if (user != null)
                {
                    if (!(await IsCurrentUser(user)))
                    {
                        return StatusCode(401);
                    }
                    IdentityResult result = await _userServices.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        user.DateOfPasswordChange = DateTime.Now;
                        await _userServices.UpdateAsync(user);

                        return RedirectToAction("UsersList");
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
                ModelState.AddModelError(string.Empty, "User not found");
            }
            return View(model);
        }
    }
}