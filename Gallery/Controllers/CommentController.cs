﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Gallery.Models.PictureViewModels;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Gallery.Controllers
{
    [Authorize]
    public class CommentController : BaseController
    {
        private readonly IServices<Comment> _commentServices;
        private readonly IPictureServices<Picture> _pictureServices;

        public CommentController(IServices<Comment> commentServices, IPictureServices<Picture> pictureServices, IUserServices<User> userServices)
            : base(userServices)
        {
            _commentServices = commentServices;
            _pictureServices = pictureServices;
            _userServices = userServices;
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetComments(Guid pictureId)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            PictureViewModel model = Mapper.Map<Picture, PictureViewModel>(picture);
            foreach (var commentViewModel in model.Comments)
            {
                commentViewModel.UserPseudonym = (await _userServices.FindByIdAsync(commentViewModel.UserId)).Pseudonym;
            }

            ViewBag.UserIsAuthenticated = User.Identity.IsAuthenticated;
            ViewBag.IsAdmin = CurrentUserIsAdmin();
            ViewBag.CurrentUserId = await CurrentUserId();
            ViewBag.OwnerPseudonym = (await _userServices.FindByIdAsync(picture.UserId)).Pseudonym;

            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(Guid pictureId, string commentText)
        {
            Picture picture = await _pictureServices.GetAsync(pictureId);
            User user = await _userServices.FindByNameAsync(User.Identity.Name);

            Comment comment = new Comment
            {
                Context = commentText,
                PostDateTime = DateTime.Now,
                PictureId = picture.Id,
                Picture = picture,
                UserId = user.Id
            };

            await _commentServices.AddAsync(comment);
            return RedirectToAction("GetComments", new { pictureId });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteComment(Guid commentId)
        {
            Comment comment = await _commentServices.GetAsync(commentId);
            Guid pictureId = comment.PictureId;
            User user = await _userServices.FindByIdAsync(comment.UserId);
            if (!(await IsAdminOrCurrentUser(user)) && ((await _pictureServices.GetAsync(pictureId)).UserId != await CurrentUserId()))
            {
                return StatusCode(401);
            }
            await _commentServices.DeleteAsync(comment);

            return RedirectToAction("GetComments", new { pictureId });
        }
    }
}