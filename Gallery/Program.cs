﻿using System;
using System.IO;
using System.Threading.Tasks;
using GalleryServices.Services;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Gallery
{
    public class Program
    {
        public static IConfiguration Configuration { get; set; }

        public static async Task Main(string[] args)
        {
            var host = BuildWebHost(args);

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            string adminEmail = Configuration.GetValue<string>("AdminInitialization:AdminEmail");
            string adminPassword = Configuration.GetValue<string>("AdminInitialization:AdminPassword");

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var userServices = services.GetRequiredService<IUserServices<User>>();
                    var roleServices = services.GetRequiredService<IRoleServices<Role>>();
                    await RoleInitializer.InitializeAsync(userServices, roleServices, adminEmail, adminPassword);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
