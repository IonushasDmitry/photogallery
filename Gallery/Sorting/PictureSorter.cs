﻿using System.Collections.Generic;
using System.Linq;
using GalleryStorage.Models;

namespace Gallery.Sorting
{
    public class PictureSorter
    {
        public IQueryable<Picture> Pictures { get; set; }
        public PictureSortState SortOrder { get; set; }

        public PictureSorter()
        {
            Pictures = new List<Picture>().AsQueryable();
            SortOrder = PictureSortState.PostDateTimeDesc;
        }

        public IQueryable<Picture> Sort()
        {
            switch (SortOrder)
            {
                case PictureSortState.UserAsc:
                    Pictures = Pictures.OrderBy(x => x.UserId);
                    break;
                case PictureSortState.UserDesc:
                    Pictures = Pictures.OrderByDescending(x => x.UserId);
                    break;
                case PictureSortState.PostDateTimeAsc:
                    Pictures = Pictures.OrderBy(x => x.PostDateTime);
                    break;
                case PictureSortState.RatingAsc:
                    Pictures = Pictures.OrderBy(x => x.Ratings.Count);
                    break;
                case PictureSortState.RatingDesc:
                    Pictures = Pictures.OrderByDescending(x => x.Ratings.Count);
                    break;
                default:
                    Pictures = Pictures.OrderByDescending(x => x.PostDateTime);
                    break;
            }

            return Pictures;
        }
    }
}
