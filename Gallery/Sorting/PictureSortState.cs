﻿namespace Gallery.Sorting
{
    public enum PictureSortState
    {
        UserAsc,
        UserDesc,
        PostDateTimeAsc,
        PostDateTimeDesc,
        RatingAsc,
        RatingDesc
    }
}
