﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Gallery.Hubs
{
    public class RatingHub: Hub
    {
        //public async Task Send(string message = null)
        //{
        //    //await Clients.All.InvokeAsync("Send", message);
        //    await Clients.All.SendAsync("Send", message);
        //}

        public async Task Send()
        {
            await Clients.All.SendAsync("Send");
        }
    }
}
