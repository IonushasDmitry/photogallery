﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Gallery.Hubs
{
    public class CommentHub: Hub
    {
        public async Task Send()
        {
            await Clients.All.SendAsync("Send");
        }
    }
}
