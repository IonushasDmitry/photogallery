﻿using System;
using System.Linq;
using AutoMapper;
using Gallery.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GalleryServices.Services;
using GalleryStorage;
using GalleryStorage.Models;
using Gallery.Models.PictureViewModels;
using Gallery.Models.RolesManagingViewModels;
using Gallery.Models.UserManagingViewModels;
using GalleryServices.ServicesInterfaces;
using GalleryStorage.Storages;
using GalleryStorage.StoragesInterfaces;

namespace Gallery
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddSingleton(Configuration);
            services.AddSignalR();
            services.AddTransient<IEmailServices, EmailServices>();

            services.AddDbContext<GalleryContext>(options =>
                options.UseSqlServer(connection));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<GalleryContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                options.User.RequireUniqueEmail = true;

            });

            services.AddTransient<IUserStorage<User>, UserStorage>();
            services.AddTransient<IUserServices<User>, UserServices>();

            services.AddTransient<IRoleStorage<Role>, RoleStorage>();
            services.AddTransient<IRoleServices<Role>, RoleServices>();

            services.AddScoped<ISignInServices<User>, SignInServices>();

            services.AddTransient<IStorage<Album>, AlbumStorage>();
            services.AddTransient<IAlbumServices<Album>, AlbumServices>();

            services.AddTransient<IStorage<Picture>, PictureStorage>();
            services.AddTransient<IPictureServices<Picture>, PictureServices>();

            services.AddTransient<IStorage<Rating>, RatingStorage>();
            services.AddTransient<IServices<Rating>, RatingServices>();

            services.AddTransient<IStorage<Comment>, CommentStorage>();
            services.AddTransient<IServices<Comment>, CommentServices>();

            services.AddTransient<ITagStorage<Tag>, TagStorage>();
            services.AddTransient<ITagServices<Tag>, TagServices>();

            Mapper.Initialize(
                cfg =>
                {
                    cfg.CreateMap<User, UserViewModel>()
                        .ForMember(x => x.IsLockedOut, opt => opt.Ignore());
                    cfg.CreateMap<User, LockUserViewModel>();

                    cfg.CreateMap<UserRegistrationViewModel, User>()
                        .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));
                    cfg.CreateMap<EditUserViewModel, User>()
                        .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email));

                    cfg.CreateMap<Role, RoleViewModel>();
                    cfg.CreateMap<RoleViewModel, Role>();

                    cfg.CreateMap<Album, AlbumViewModel>();
                    cfg.CreateMap<Album, ChangeAlbumViewModel>();
                    cfg.CreateMap<Picture, PictureViewModel>()
                        .ForMember(dto => dto.TagNames,
                            src => src.MapFrom(x => x.PictureTags.Select(y => y.Tag.Name).ToList()));
                    cfg.CreateMap<Comment, CommentViewModel>();
                });

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<RatingHub>("/RatingHub");
                routes.MapHub<CommentHub>("/CommentHub");
            }
            );

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
        }
    }
}
